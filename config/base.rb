require 'rubygems'
require 'active_record'
require 'csv'
$: << "lib"
require "active_csv"
require 'commit'

ActiveRecord::Base.establish_connection({
  :adapter => "sqlite3",
  :database => ENV['DB_ENV'] && ENV['DB_ENV'] + '.db' || "local.db",
})

