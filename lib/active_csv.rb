require 'csv'

class ActiveCsv
  class << self
    attr_reader :attributes

    def read_from(filename)
      @objects = []
      CSV::Reader.parse(File.open(filename)) do |row|
        if @attributes
          object = self.new
          @attributes.each_with_index do |attr, index|
            object.send("#{attr}=", row[index])
          end
          @objects << object
          yield(object) if block_given?
        else
          @attributes = row
          @attributes.each { |attr| attr_accessor attr }
        end
      end
    end

    include Enumerable

    def each(&block)
      @objects.each(&block)
    end

  end
end
