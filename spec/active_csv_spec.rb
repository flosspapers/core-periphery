require 'active_csv'

describe ActiveCsv do

  def class_for(filename)
    klass = Class.new(ActiveCsv)
    klass.instance_eval do
      read_from(File.join(File.dirname(__FILE__), filename))
    end
    klass
  end

  before(:all) do
    @Sample = class_for('sample.csv')
  end

  it 'reads column names from CSV header' do
    class_for('sample.csv').attributes.should include("id","name","email")
  end

  it 'creates accessors for fields in header' do
    object = @Sample.new
    object.should respond_to(:id)
    object.should respond_to(:name)
    object.should respond_to(:email)
  end

  it 'exposes rows as objects' do
    object = @Sample.first
    object.id.should == "1"
    object.name.should == "Antonio Terceiro"
    object.email.should == "terceiro@softwarelivre.org"
  end

  it "parses quoted strings" do
    object = @Sample.find { |obj| obj.id == "2" }
    object.name.should == "Quoted Person"
  end

end
