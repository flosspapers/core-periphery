% vim: ft=tex

Normally, a Free Software project is started by a single developer, or
by a group of developers, in need of addressing a particular need. After
there is a usable version, it is released to the public under a Free
Software license which allows anyone to use, change and distribute a
copy of that software. As new users get interested in the project, some
of them may start to contribute to it in several possible ways: with
code for new features or bug fixes, with translations into their native
languages, with documentation, or with other types of contribution.
At some point, then, the project has a vivid and active community: a
group of people that gravitate around a project, with varied levels of
involvement and contribution.

The ``onion model'' \cite{crowston2005, mockus2002} became a widely
accepted representation of what happens in a Free Software project, by
indicating the existence of concentric levels of contribution: a small
group of core developers do the largest part of the work; a larger group
makes direct, but less frequent contributions in the form of bug fixes,
patches, documentation, etc; an even larger group reports problems as
they use the software, and the largest group is formed by the silent
users who only use the software but never provide any type of feedback.

The processes by which participants migrate from one group to another
are very different from one community to the other: communities may
adopt more formal and explicit procedures for that, or use a more
relaxed approach and let things flow ``naturally''. But in general the
achievement of central roles (and thus more responsibility, respect and
decision power) are merit-based: a developer becomes a leader by means
of continuous valuable contributions to the community \cite{jensen2007}.

Since most of the work is done by a core team, it is important for
projects to keep a healthy and active core team. Some projects are able
to keep its core team with few or no changes across its entire history,
while others experience a succession of different generations of core
developers \cite{robles2006, robles2009}.

The relationship between core contributors and peripheral (non-core)
members of a community are not always smooth: sometimes the core tends
to work on their own demands and to give little attention or even
to ignore completely the demands of the periphery \cite{dalle2008,
masmoudi2009}. From an individual point of view, core and periphery
members also exhibit different behaviour while debating subjects related
to the project \cite{scialdone2009} or in the bug reporting activity
\cite{masmoudi2009}.

