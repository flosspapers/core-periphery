NAME = text
TEXSRCS = core-periphery-structural-complexity.tex core-periphery.tex
BIBTEXSRCS = references.bib
CLEAN_FILES += *.snm *.nav

include figures.mk

include /usr/share/latex-mk/latex.gmk

figures.mk: ~/research/figures.mk
	cp $< $@

cpsc.tar.gz:
	tar --exclude=.git -czf $@ *

publish: cpsc.tar.gz
	scp $< homes.dcc.ufba.br:public_html/papers/

clean::
	rm -f cpsc.tar.gz *.log
