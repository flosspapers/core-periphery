class Commit < ActiveRecord::Base

  NUMBER_OF_PERIODS = ENV['NUMBER_OF_PERIODS'] ? ENV['NUMBER_OF_PERIODS'].to_i : 20
  CORE_PERCENT = ENV['CORE_PERCENT'] ? ENV['CORE_PERCENT'].to_f : 20.0

  belongs_to :previous, :class_name => 'Commit'
  #named_scope :relevant, :include => :previous, :conditions => 'previous_commits.structural_complexity != commits.structural_complexity'
  named_scope :by_project, lambda { |project| { :conditions => ['commits.project = ?', project] } }
  named_scope :by_period, lambda { |project, n| { :conditions => { :project => project, :date => period(project, n)} } }
  def self.projects
    self.connection.execute("select distinct(project) from commits").map { |row| row['project'] }
  end
  def self.start_date(project)
    @start_date ||= {}
    @start_date[project] ||= self.by_project(project).first(:order => 'date').date
  end
  def self.end_date(project)
    @end_date ||= {}
    @end_date[project] ||= self.by_project(project).last(:order => 'date').date
  end
  def self.period(project, n)
    project_first_day = start_date(project)
    seconds_by_period = (end_date(project) - project_first_day) / NUMBER_OF_PERIODS
    Range.new(project_first_day + ((n-1)*seconds_by_period + 1).seconds, project_first_day + (n*seconds_by_period).seconds)
  end
  def self.committers_by_period(project, period)
    @committers_by_period ||= {}
    key = "#{project}/#{period}"
    @committers_by_period[key] ||= Commit.by_period(project, period).find(:all, :select => 'author_name, count(*)', :group => :author_name, :order => 'count(*) DESC').map(&:author_name)
  end

  def period
    project_first_day = self.class.start_date(project)
    seconds_by_period = (self.class.end_date(project) - project_first_day) / NUMBER_OF_PERIODS
    seconds = (date - project_first_day)
    (seconds / seconds_by_period).ceil
  end
  def core?
    committers = self.class.committers_by_period(project, period)
    ntop = (committers.size.to_f * CORE_PERCENT / 100.0).ceil
    top = committers.first(ntop)
    top.include?(author_name)
  end

  def do_calculations
    if (self.date)
      self.normalized_date = (self.date.to_f - self.class.start_date(project).to_f) / (self.class.end_date(project).to_f - self.class.start_date(project).to_f)
    end
    self.core = self.core?
    if previous
      self.delta_structural_complexity = structural_complexity - previous.structural_complexity
      self.absolute_delta_structural_complexity = self.delta_structural_complexity.abs
    end
  end
end

