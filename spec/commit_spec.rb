require 'spec_helper'

describe Commit do
  it 'calculates normalized date' do
    Commit.stub(:start_date).and_return(Date.new(2009, 1, 1).to_time)
    Commit.stub(:end_date).and_return(Date.new(2010, 1, 1).to_time)

    [
      [Date.new(2009, 7, 1), 0.5],
      [Date.new(2009, 1, 1), 0.0],
      [Date.new(2010, 1, 1), 1.0],
    ].each do |date, n|
      commit = Commit.new(:date => date.to_time)
      commit.do_calculations
      commit.normalized_date.should be_close(n, 0.01)
    end
  end

  it 'calculates level of involvement' do
    commit = Commit.new
    [true, false].each do |status|
      commit.stub(:core?).and_return(status)
      commit.do_calculations
      commit.core.should == status
    end
  end

  it 'calculates delta structural complexity' do
    [
      [2.0, 1.5, 0.5, 0.5],
      [2.0, 2.5, -0.5, 0.5],
    ].each do |in1, in2, out, abs_out|
      commit = Commit.new(:structural_complexity => in1)
      commit.previous = Commit.new(:structural_complexity => in2)
      commit.stub(:core?)
      commit.do_calculations
      commit.delta_structural_complexity.should be_close(out, 0.01)
      commit.absolute_delta_structural_complexity.should be_close(abs_out, 0.01)
    end
  end
end
