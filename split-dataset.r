# exclude the changes that do not alter the Structural Complexity
relevant <- subset(changes, MDSC > 0)

# calculate subsets for core and periphery
core <- subset(relevant, L == 't')
periphery <- subset(relevant, L == 'f')

core_reduction <- subset(core, DSC < 0)
periphery_reduction <- subset(periphery, DSC < 0)